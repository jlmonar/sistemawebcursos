﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Library
{
    public class ExportData : Controller
    {
        private List<String[]> _listData;
        private String _fileName, _table;
        private String[] _titles;
        private IWebHostEnvironment _environment;

        public ExportData(List<String[]> listData, String fileName, String table, String[] titles, IWebHostEnvironment environment)
        {
            _listData = listData;
            _fileName = fileName;
            _table = table;
            _titles = titles;
            _environment = environment;
        }

        public async Task<IActionResult> ExportDataAsync()
        {
            //Obtengo la dirección de la carpeta wwwroot del proyecto.
            string sWebRootFolder = _environment.WebRootPath;

            //Creamos el siguiente objeto que permitirá almacenar información en memoria.
            var memory = new MemoryStream();

            //Usamos FileStream para poder obtener/crear un archivo y guardarlo en memoria, primer parámetro es el directorio
            //donde se guardará/obtendrá el archivo, segundo parámetro es el modo de creación, y tercero son los permisos read/write.
            using(var fs = new FileStream(Path.Combine(sWebRootFolder, _fileName), FileMode.Create, FileAccess.Write))
            {
                //Instalo la última versión de IWorkbook.
                IWorkbook workbook;
                //Creo el documento excel.
                workbook = new XSSFWorkbook();

                //Creo una hoja/libro para el documento de excel y le asigno como nombre a la hoja el nombre de la tabla.
                ISheet excelSheet = workbook.CreateSheet(_table);
                //Creo una fila en la hoja/libro creada en la posición 0
                IRow row = excelSheet.CreateRow(0);

                //Creo las celdas para la primer fila de la hoja, y estas tendrán como valor el nombre de los campos
                //de la tabla.
                for(int i = 0; i < _titles.Length; i++)
                {
                    //Creo la celda en la posición i del row y le asigno un valor a dicha celda.
                    row.CreateCell(i).SetCellValue(_titles[i]);
                }

                //Inicio count en 1 porque en la posicion 0 ya fue creado un row que contiene los nombres
                //de las columnas de la tabla que queremos exportar.
                int count = 1;

                //Creo las celdas para las filas restantes de la hoja (a partir de la fila 1) y estan tendrán la data o registros
                //de la tabla que queremos exportar
                for (int i = 0; i < _listData.Count; i++)
                {
                    row = excelSheet.CreateRow(count);
                    var arrayData = _listData[i];

                    //for(int j = 0; j < arrayData.Length; j++)
                    for (int j = 0; j < _titles.Length; j++)
                    {
                        row.CreateCell(j).SetCellValue(arrayData[j]);
                    }

                    count++;
                }
                //Creamos/escribimos la hoja de excel en el objeto fs(filestream) que creamos. El archivo se creará
                //dentro de la carpeta wwroot del proyecto
                workbook.Write(fs);
            }

            //Luego de haber guardado el archivo de excel, ahora leemos/abrimos el documento (en la misma ruta que se 
            //lo guardó) para poder descargarlo desde el navagador.
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, _fileName), FileMode.Open))
            {
                //Para poder descargarlo, copiamos el archivo excel (stream) en el objeto memory que creamos casi al inicio.
                //Como parámetro se envia el stream destino donde se va a copiar la información
                await stream.CopyToAsync(memory);
            }

            //Obtenemos la información que esté en la posición 0 de memory.
            memory.Position = 0;

            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            //Generamos el archivo que se quiere descargar.
            return File(memory, contentType, _fileName);
        }

    }
}
