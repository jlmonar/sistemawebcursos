﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaWebCursos.Areas.Categorias.Models;
using SistemaWebCursos.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Library
{
    public class LCategorias
    {
        private ApplicationDbContext _context;

        public LCategorias(ApplicationDbContext context)
        {
            _context = context;
        }

        public IdentityError RegistrarCategoria(TCategoria categoria)
        {
            IdentityError identityError;
            try
            {
                //Si no llega el id de la categoria, entonces hacemos un insert a la abse de datos.
                if (categoria.CategoriaID.Equals(0))
                {
                    _context.Add(categoria);
                } else
                {
                    //Si llega un id, entonces la categoria existe y la tenemos que actualizar.
                    _context.Update(categoria);
                }
                
                _context.SaveChanges();
                identityError = new IdentityError { Code = "Done" };
            }
            catch (Exception e)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }

        /// <summary>
        /// Obtiene la lista de categorias desde la base de datos, ya sean todos los registros, o filtrado 
        /// si se pasa como parámetro un texto de búsqueda
        /// </summary>
        /// <param name="valor">Texto de bpusqueda</param>
        /// <returns>Lista de categorias</returns>
        public List<TCategoria> getTCategorias(String valor)
        {
            List<TCategoria> listCategorias;

            //Si el valor es null, quiere decir que no se pasó como parametro ningún texto de búsqueda,
            //por lo tanto, obtengo todas las categorias registradas.
            if (valor == null)
            {
                //Hago consulta a la base de datos y obtengo todas las categorias.
                listCategorias = _context._TCategoria.ToList();
            } else
            {
                //Hago consulta a la base de datos y filtro los registros segpun el valor (o texto de búsqueda)
                //proporcionado por el usuario.
                listCategorias = _context._TCategoria.Where(c => c.Nombre.StartsWith(valor)).ToList();
            }

            return listCategorias;
        }

        internal IdentityError UpdateEstado(int id)
        {
            IdentityError identityError;
            try
            {
                var categoria = _context._TCategoria.Where(c => c.CategoriaID.Equals(id)).ToList().ElementAt(0);
                //Esta también es otra forma mas fácil de obtener la categoria.
                //var categoria = _context._TCategoria.Find(id);

                //Cambio el valor del estado.
                categoria.Estado = categoria.Estado ? false : true;

                //Guardo los cambios en la base de datos
                _context.Update(categoria);
                _context.SaveChanges();

                identityError = new IdentityError { Code = "Done", Description = "Actualización exitosa del estado." };
            }
            catch (Exception e)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }

        internal IdentityError DeleteCategoria(int categoriaID)
        {
            IdentityError identityError;
            try
            {
                //Obtengo la categoria que quiero eliminar.
                var categoria = _context._TCategoria.Find(categoriaID);

                //Elimino la categoria en la base ded datos.
                _context.Remove(categoria);
                _context.SaveChanges();

                identityError = new IdentityError { Code = "Done" };
            }
            catch (Exception e)
            {
                identityError = new IdentityError { Code = "Error", Description = e.Message };
            }

            return identityError;
        }

        public List<SelectListItem> getTCategorias()
        {
            var _selectList = new List<SelectListItem>();
            var categorias = _context._TCategoria.Where(c => c.Estado.Equals(true)).ToList();

            foreach (var item in categorias)
            {
                _selectList.Add(new SelectListItem {
                    Text = item.Nombre,
                    Value = item.CategoriaID.ToString()
                });
            }

            return _selectList;
        }
    }
}
