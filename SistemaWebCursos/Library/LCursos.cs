﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using SistemaWebCursos.Areas.Cursos.Models;
using SistemaWebCursos.Areas.Inscripciones.Models;
using SistemaWebCursos.Data;
using SistemaWebCursos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Library
{
    public class LCursos
    {
        UploadImage _image;
        private ApplicationDbContext context;
        private IWebHostEnvironment environment;

        public LCursos(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
            _image = new UploadImage();
        }

        public async Task<IdentityError> RegistrarCursoAsync(DataPaginador<TCursos> model)
        {
            IdentityError identityError;
            String messageDescription;

            try
            {
                //Si CursoID es 0, significa que tenemos que crear el curso
                if (model.Input.CursoID.Equals(0))
                {
                    var imageByte = await _image.ByteAvatarImageAsync(model.AvatarImage, environment);

                    var curso = new TCursos()
                    {
                        Nombre = model.Input.Nombre,
                        Descripcion = model.Input.Descripcion,
                        Costo = model.Input.Costo,
                        Horas = model.Input.Horas,
                        Estado = model.Input.Estado,
                        Image = imageByte,
                        CategoriaID = model.Input.CategoriaID
                    };
                    context.Add(curso);
                    context.SaveChanges();

                    messageDescription = "Curso Guardado con exito.";
                } else
                {
                    //Si CursoID es mayor a 0, entonces la petición vino con un id de curso, por lo tanto setrata de una
                    //actualización de curso.

                    byte[] imageByte;
                    if (model.AvatarImage != null)
                    {
                       imageByte = await _image.ByteAvatarImageAsync(model.AvatarImage, environment);
                    } else
                    {
                        imageByte = model.Input.Image;
                    }

                    var curso = new TCursos()
                    {
                        CursoID = model.Input.CursoID,
                        Nombre = model.Input.Nombre,
                        Descripcion = model.Input.Descripcion,
                        Costo = model.Input.Costo,
                        Horas = model.Input.Horas,
                        Estado = model.Input.Estado,
                        CategoriaID = model.Input.CategoriaID,
                        Image = imageByte
                    };

                    context.Update(curso);
                    context.SaveChanges();

                    messageDescription = "Curso Actualizado con exito.";
                }

                identityError = new IdentityError() { Code = "Done", Description = messageDescription };
            }
            catch (Exception e)
            {
                identityError = new IdentityError()
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }

        internal List<TCursos> getTCursos(string search)
        {
            List<TCursos> listaCursos;
            if (search == null)
            {
                listaCursos = context._TCurso.ToList();
            } else
            {
                listaCursos = context._TCurso.Where(c => c.Nombre.Contains(search)).ToList();
            }

            return listaCursos;
        }

        internal IdentityError DeleteCurso(int cursoID)
        {
            IdentityError identityError;
            try
            {
                var curso = context._TCurso.Find(cursoID);
                context.Remove(curso);
                context.SaveChanges();

                identityError = new IdentityError()
                {
                    Code = "Done",
                    Description = "Curso eliminado con exito."
                };
            }
            catch (Exception e)
            {
                identityError = new IdentityError()
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }

        internal IdentityError UpdateEstado(int id)
        {
            IdentityError identityError;
            try
            {
                var curso = context._TCurso.Find(id);

                curso.Estado = curso.Estado ? false : true;
                context.Update(curso);
                context.SaveChanges();

                identityError = new IdentityError()
                {
                    Code = "Done",
                    Description = "Estado actualizado con exito."
                };
            }
            catch (Exception e)
            {
                identityError = new IdentityError()
                {
                    Code = "Error",
                    Description = e.Message
                };
            }
            return identityError;
        }

        public DataCurso GetTCurso(int id)
        {
            DataCurso dataCurso = null;

            var query = context._TCategoria.Join(context._TCurso,
                c => c.CategoriaID,
                t => t.CategoriaID,
                (c,t) => new {
                    CategoriaID = c.CategoriaID,
                    NombreCategoria = c.Nombre,
                    CursoID = t.CursoID,
                    NombreCurso = t.Nombre,
                    DescripcionCurso = t.Descripcion,
                    Horas = t.Horas,
                    Costo = t.Costo,
                    Estado = t.Estado,
                    Image = t.Image
                }).Where(d => d.CursoID.Equals(id)).ToList();

            if (query.Count > 0)
            {
                //Obtengo la data del resultado obtenido de la consulta.
                var data = query.First();

                dataCurso = new DataCurso
                {
                    CursoID = data.CursoID,
                    Nombre = data.NombreCurso,
                    Descripcion = data.DescripcionCurso,
                    Horas = data.Horas,
                    Costo = data.Costo,
                    Estado = data.Estado,
                    Image = data.Image,
                    Categoria = data.NombreCategoria
                };
            }

            return dataCurso;
        }

        public IdentityError InscripcionEstudianteCurso(int cursoID, String idUser)
        {
            IdentityError identityError;

            try
            {
                var inscripcionCurso = context._TInscripcion.Where(i => i.CursoID.Equals(cursoID) && i.EstudianteID.Equals(idUser)).ToList();

                if (inscripcionCurso.Count.Equals(0))
                {//El estudiante no se encuentra inscrito en el curso.
                    var curso = context._TCurso.Find(cursoID);

                    var inscripcion = new Inscripcion
                    {
                        CursoID = curso.CursoID,
                        EstudianteID = idUser,
                        Fecha = DateTime.Now,
                        Pago = curso.Costo
                    };

                    context.Add(inscripcion);
                    context.SaveChanges();

                    identityError = new IdentityError
                    {
                        Code = "Done",
                        Description = "Estudiante inscrito al curso con exito."
                    };
                } else
                {
                    identityError = new IdentityError
                    {
                        Code = "Error",
                        Description = "El estudiante ya se encuentra registrado en el curso."
                    };
                }
            }
            catch (Exception e)
            {
                identityError = new IdentityError
                {
                    Code = "Error",
                    Description = e.Message
                };
            }

            return identityError;
        }

        public List<DataCurso> InscripcionesEstudiante(String idUser, String search)
        {
            List<DataCurso> cursos = new List<DataCurso>();

            //Obtengo las inscripciones del estudiante
            var inscripciones = context._TInscripcion.Where(i => i.EstudianteID.Equals(idUser)).ToList();

            if (inscripciones.Count > 0)
            {//Si es mayor a 0, entonces si hay datos en la consulta y el estudiante está registrado en algún curso.
                //Recorro lalista de isncripciones.
                inscripciones.ForEach(i =>
                {
                    if (search == null || search == "")
                    {//Si no se pasó texto de búsqueda, se obtiene el dato cada curso en la lista
                     //Obtengo los datos del curso de cada Inscripcion de la lista obtenida previamente.
                        var query = context._TCategoria.Join(context._TCurso,
                            c => c.CategoriaID,
                            t => t.CategoriaID,
                            (c, t) => new
                            {
                                CategoriaID = c.CategoriaID,
                                NombreCategoria = c.Nombre,
                                CursoID = t.CursoID,
                                NombreCurso = t.Nombre,
                                DescripcionCurso = t.Descripcion,
                                Horas = t.Horas,
                                Costo = t.Costo,
                                Estado = t.Estado,
                                Image = t.Image
                            }).Where(d => d.CursoID.Equals(i.CursoID)).ToList();
                        //Con i.CursoID estoy haciendo referencia al CursoID que está en el registro Inscripcion de la lista de inscripciones

                        if (query.Count > 0)
                        {
                            var data = query.First();

                            //Creo el objeto DataCurso con los datos obtenidos en el query para buscar el curso.
                            var curso = new DataCurso
                            {
                                CursoID = data.CursoID,
                                Nombre = data.NombreCurso,
                                Descripcion = data.DescripcionCurso,
                                Costo = data.Costo,
                                Estado = data.Estado,
                                Horas = data.Horas,
                                Image = data.Image,
                                Categoria = data.NombreCategoria
                            };

                            //Agrego el curso a lia lista de DataCurso
                            cursos.Add(curso);
                        }
                    }
                    else
                    {
                        //Si el usuario envia un texto de busqueda, entonces se hace una pequeña variación a la consulta.

                        var query = context._TCategoria.Join(context._TCurso,
                           c => c.CategoriaID,
                           t => t.CategoriaID,
                           (c, t) => new
                           {
                               NombreCategoria = c.Nombre,
                               CursoID = t.CursoID,
                               NombreCurso = t.Nombre,
                               DescripcionCurso = t.Descripcion,
                               Horas = t.Horas,
                               Costo = t.Costo,
                               Estado = t.Estado,
                               Image = t.Image
                           }).Where(d => d.CursoID.Equals(i.CursoID) && d.NombreCurso.Contains(search)).ToList();
                        //Con i.CursoID estoy haciendo referencia al CursoID que está en el registro Inscripcion de la lista de inscripciones

                        if (query.Count > 0)
                        {
                            var data = query.First();

                            //Creo el objeto DataCurso con los datos obtenidos en el query para buscar el curso.
                            var curso = new DataCurso
                            {
                                Categoria = data.NombreCategoria,
                                CursoID = data.CursoID,
                                Nombre = data.NombreCurso,
                                Descripcion = data.DescripcionCurso,
                                Costo = data.Costo,
                                Estado = data.Estado,
                                Horas = data.Horas,
                                Image = data.Image
                            };

                            //Agrego el curso a lia lista de DataCurso
                            cursos.Add(curso);
                        }
                    }
                });
            }

            return cursos;
        }
    }
}
