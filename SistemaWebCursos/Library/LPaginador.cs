﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Library
{
    public class LPaginador<T>
    {
        //Cantidad de resultados por página.
        private int pagi_cuantos = 6;
        //Número de enlaces que se mostraan como máximo en la barra de navegación.
        private int pagi_nav_num_enlaces = 3;
        //Página actual en la que nos encontramos.
        private int pagi_actual;
        //Definimos el texto que irá en el enlace a la página anterior.
        private String pagi_nav_anterior = " &laquo; Anterior";
        //Definimos el texto que irá en el enlace a la página siguiente.
        private String pagi_nav_siguiente = "Siguiente &raquo;";
        //Definimos el texto que irá en el enlace a la primera página.
        private String pagi_nav_primera = " &laquo; Primero";
        //Definimos el texto que irá en el enlace a la última página.
        private String pagi_nav_ultima = "Ultimo &raquo;";
        private String pagi_navegacion = null;

        /// <summary>
        /// Permite crear un paginador para los registros de cualquer tipo de objeto, clase o tabla.
        /// </summary>
        /// <param name="table">Colección de objetos de la clase genérica</param>
        /// <param name="pagina">Número de página del paginador que se solicita visualizar</param>
        /// <param name="Registros">Número de registros que se deben mostrar por página</param>
        /// <param name="area">Area del modelo de los cuales queremos paginar sus registros</param>
        /// <param name="controller">Controller del modelo de los cuales queremos paginar sus registros</param>
        /// <param name="action">Action del controller donde se ubica el modelo de los cuales queremos paginar sus registros</param>
        /// <param name="host">Url del sitio</param>pagi_info, pagi_navegacion, query
        /// <returns>Array de objetos que contiene, String con información de la paginación, String con link de las navegaciones, y la colección de categorias que se mostrarán en la vista</returns>
        public object[] paginador (List<T> table, int pagina, int Registros, String area, String controller, String action, String host)
        {
            if (Registros > 0)
            {
                pagi_cuantos = Registros;
            }

            if (pagina.Equals(0))
            {
                //Si no se ha hecho click a ninguna págian especifica, es decir,
                //si es la primera vez que se ejecuta el script entonces
                //pagi_actual es la página actual, será por defecto la primera.

                pagi_actual = 1;
            } else
            {
                //Si se solicitó una página específica
                //entonces pagi_actual será la que se pidió.
                pagi_actual = pagina;
            }
            /*
            int pagi_totalReg = table.Count;
            int pagi_totalRegs = pagi_totalReg;

            if ((pagi_totalReg % pagi_cuantos) > 0)
            {
                pagi_totalRegs += 2;
            }
            int pagi_totalPags = pagi_totalRegs / pagi_cuantos;
            */
            int pagi_totalReg = table.Count;

            double valor1 = Math.Ceiling((double) pagi_totalReg / (double) pagi_cuantos);

            int pagi_totalPags = Convert.ToInt16(Math.Ceiling(valor1));

            if (pagi_actual != 1)
            {
                //Si no estamos en la primera página, ponemos el enlace de la primera página.
                int pagi_url = 1; // Es el número de página al cual enlazamos
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action +
                    "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" + pagi_nav_primera + "</a>";

                //Si no estamos en la página 1, ponemos el enlace anterior
                pagi_url = pagi_actual - 1; // Es el número de página al cual enlazamos
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" + action +
                    "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" + pagi_nav_anterior + "</a>";

            }

            //Si se definió la variable pagi_nav_num_enlaces, entonces calculamos
            //el intervalo para restar y sumar a partir de la página actual.
            double valor2 = (pagi_nav_num_enlaces / 2);
            int pagi_nav_intervalo = Convert.ToInt16(Math.Round(valor2));
            //Calculamos desde que página se mostrará.
            int pagi_nav_desde = pagi_actual - pagi_nav_intervalo;
            //Calculamos hasta que página se mostrará.
            int pagi_nav_hasta = pagi_actual + pagi_nav_intervalo;

            //Si pagi_nav_desde es un número negativo.
            if (pagi_nav_desde < 1)
            {
                //Le sumamos la cantidad sobrante al final para mentener el número
                //de enlaces que se quieren mostrar.
                pagi_nav_hasta -= (pagi_nav_desde - 1);
                //Establecemos pagi_nav_desde como 1.
                pagi_nav_desde = 1;
            }

            //Si pagi_nav_hasta es un npumero mayor que el total de páginas.
            if (pagi_nav_hasta > pagi_totalPags)
            {
                //Le restamos la cantidad excedida al comienzo para manteneer el
                //número de enlaces que se quiere mostrar.
                pagi_nav_desde -= (pagi_nav_hasta - pagi_totalPags);
                //Establecemos pagi_nav_hasta como el total de páginas.
                pagi_nav_hasta = pagi_totalPags;

                //Hacemos el último ajuste verificando que al cambiar pagi_nav_desde
                //no haya quedado con un valor no válido.
                if (pagi_nav_desde < 1)
                {
                    pagi_nav_desde = 1;
                }
            }

            for (int pagi_i = pagi_nav_desde; pagi_i <= pagi_nav_hasta; pagi_i++)
            {
                //Desde página 1 hasta última página (pagi_totalPags) 
                if (pagi_i == pagi_actual)
                {
                    //Si el número de página es la actual (pagi_actual), se excribe el número, perso sin enlace.
                    pagi_navegacion += "<span class='btn btn-default' disable='disabled'>" + pagi_i + "</span>";
                }
                else
                {
                    //Si es cualquier otro, se escribe el enlace a dicho npumero de página.
                    pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/"
                        + action + "?id=" + pagi_i + "&Registros=" + pagi_cuantos + "&area=" + area + "'>"
                        + pagi_i + "</a>";
                }
            }

            if (pagi_actual < pagi_totalPags)
            {
                //Si no estamos en la última página, ponemos el enlace "Siguiente".
                int pagi_url = pagi_actual + 1; //Número de página al que enlazamos.
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" +
                    action + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" +
                    pagi_nav_siguiente + "</a>";

                //Si no estamos en la última página, ponemos el enlace "Última".
                pagi_url = pagi_totalPags; //Número de página al que enlazamos.
                pagi_navegacion += "<a class='btn btn-default' href='" + host + "/" + controller + "/" +
                    action + "?id=" + pagi_url + "&Registros=" + pagi_cuantos + "&area=" + area + "'>" +
                    pagi_nav_ultima + "</a>";
            }

            /*
             * --------------------------------------------------------------------
             * Obtención de los registros que se mostrarán en la página actual.
             * --------------------------------------------------------------------
             */
            //Calculamos desde que registro se mostrará en esta página
            //Recordemos que el conteo empieza desdew cero.
            int pagi_inicial = (pagi_actual - 1) * pagi_cuantos;

            //Skip: lo que hace es omitir los primeros objetos de la colección, en este caso está omitiendo 'pagi_inicial' registros.
            //Take: lo que hace es tomar solo cierta cantidad de objetos(registros) en este caso tomará solo 'pagi_cuantos' registros de la colección resultante
            //      a partir de usar Skip
            //Por ejemplo, asumiendo que pagi_inicial = 20 y pagi_cuantos = 10, entonces, se estarían omitiendo los primeros 20 registros 
            //de la colección, y a partir de ahí se tomarán solo 10 registros, es decir, se están tomando desde el registro 21 hasta el registro 30
            var query = table.Skip(pagi_inicial).Take(pagi_cuantos).ToList();

            /*
             * --------------------------------------------------------------------
             * Generación de la información sobre los registros mostrados.
             * --------------------------------------------------------------------
             */
            //Número del primer registro de la página actual.
            int pagi_desde = pagi_inicial + 1;
            //Número del último registro de la página actual.
            int pagi_hasta = pagi_inicial + pagi_cuantos;

            if (pagi_hasta > pagi_totalReg)
            {
                //Si estamos en la última página, entonces el último registro
                //de la página actual será igual al número de registros.
                pagi_hasta = pagi_totalReg;
            }

            String pagi_info = "del <b>" + pagi_desde + "</b> al <b>" + pagi_hasta + "</b> de <b>" +
                pagi_totalReg + "</b> <b>/" + pagi_cuantos + " registros por página </b> ";

            object[] data = { pagi_info, pagi_navegacion, query };

            return data;
        }
    }
}
