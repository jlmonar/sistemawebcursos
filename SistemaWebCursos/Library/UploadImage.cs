﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Library
{
    public class UploadImage
    {
        /// <summary>
        /// Obtiene los bytes asincronamente de una imagen que ha sido subida por el usuario, si no fue subida por el usuario
        /// se obtiene los bytes de una imagen por defecto
        /// </summary>
        /// <param name="AvatarImage">Imagen cargada por el usuario en un input file de una vista</param>
        /// <param name="environment">Información sobre el entorno sobre el que está ejecutandose la aplicación</param>
        /// <returns>Array de bytes de la imagen que se va a subir (ya sea la seleccionada por el usuario, o una por defecto.)</returns>
        public async Task<byte[]> ByteAvatarImageAsync(IFormFile AvatarImage, IWebHostEnvironment environment)
        {
            if (AvatarImage != null)
            {
                using (var memorystream = new MemoryStream())
                {
                    //Con 'await' indico que el siguiente método va a ajecutar una tarea, y que se tiene
                    //que esperar hasta que termine su tarea, una vez que termina su tareea se prosigue con el código siguiente.
                    //La tarea que se va a realizar aqui es copiar la imagen(AvatarImage) hacia el buffer de memoria(memorystream), 
                    //y ya que esta es una tarea asincrona se usa el método CopiToAsync().
                    await AvatarImage.CopyToAsync(memorystream);
                    return memorystream.ToArray();
                }

            } else
            {
                //Obtengo la dirección del path del proyecto y concateno la ruta donde se encuentra de la  imagen por defecto.
                var archivo = environment.ContentRootPath + "/wwwroot/Images/logo-google.png";

                //Leo el archivo para obtener el arreglo de bytes.
                return File.ReadAllBytes(archivo);
            }
        }
    }
}
