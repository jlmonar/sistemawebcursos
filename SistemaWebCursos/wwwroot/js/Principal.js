﻿class Principal {
    /**
     * 
     * @param {any} URLactual dirección que obtendremos de la barra de direcciones del navegador (pathname), 
     * el cual contendrá el nombre del controlador y el método de acción.
     */
    userLink(URLactual) {
        let url = "";
        let cadena = URLactual.split("/");

        //Vamos a concatenar el nombre del controlador y el nombre del método de acción, sin que contenga "/".
        for (var i = 0; i < cadena.length; i++) {
            url += cadena[i];
        }

        //Si el controller y action coinciden con Cursos, entonces le agregaremos el EventListener al input file.
        if (url == "Cursos" || url == "CursosIndex") {
            //'change': Es el nombre del evento disparado por el input file.
            //'files': es el id del input desde donde seleccionamos la imagen.
            //cursoImage: es el nombre método (creado en el script inicial site.js) que se ejecutará cuando el evento ocurra.
            document.getElementById("files").addEventListener('change', cursoImage, false);
        }
    }
}