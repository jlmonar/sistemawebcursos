﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var categorias = new Categorias();

var curso = new Cursos();

/**
 *
 * @param {any} evt Evento que se dispara cuando usuario selecciona un archivo en un input file.
 */
var cursoImage = function (evt) {
    //alert("cursoImage");
    //"cursoImage" es el id del elemento 'output' en la vista Index de cursos, y lo que aquí haremos 
    //será reemplazar la etiqueta img dentro de output, por otra etiqueta img creada en el método archivo()
    curso.archivo(evt, "cursoImage");
}

var principal = new Principal();

//Método que se ejecuta siempre que se carga una página o cuando se actualiza una página.
$(document).ready(function () {
    let url = window.location.pathname;
    principal.userLink(url);
});
