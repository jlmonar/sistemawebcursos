﻿class UploadPicture {
    /**
     * 
     * @param {any} evt Evento del input file cuando el usuario selecciona un archivo.
     * @param {any} id Identificador del elemento que manipularemos para mostrar la imagen seleccionada por el usuario
     */
    archivo(evt, id) {
        let files = evt.target.files; //Lista de objetos de los archivos (input file) seleccionados por el usuario.
        let f = files[0];

        //Valido si el tipo de archivo seleccionado por el usuario es una imagen.
        if (f.type.match('image.*')) {
            let reader = new FileReader();//reader me va a permitir leer el archivo

            //Cargamos nuestro archivo.
            reader.onload = ((theFile) => {
                return (e) => {
                    document.getElementById(id).innerHTML = ['<img class="responsive-image ' + id +
                        ' " src="', e.target.result, '" tittle="', escape(theFile.name), '"/>'].join('');
                }
            })(f); // (f) es nuestro archivo, el seleccionado por el usuario.

            //readAsDataURL: Nos permite leer el archivo que estamos cargando.
            reader.readAsDataURL(f);
        }
    }
}