﻿class Cursos extends UploadPicture {
    constructor() {
        super();
        this.Image = null;
        this.CursoID = 0;
    }

    RegistrarCurso() {
        var data = new FormData();
        data.append("Input.Nombre", $("#curNombre").val());
        data.append("Input.Descripcion", $("#curDescripcion").val());
        data.append("Input.Horas", $("#curHoras").val());
        data.append("Input.Costo", $("#curCosto").val());
        data.append("Input.Estado", document.getElementById("curEstado").checked);
        data.append("Input.CategoriaID", $("#curCategoria").val());

        //Agregamos esto al formData en caso de que se trate de un curso al que estamos editando.
        data.append("Input.CursoID", $("#curCursoID").val());
        data.append("Input.Image", this.Image);

        //Añado al formData la información de la imagen seleccionada por el usuario en el input file.
        data.append("AvatarImage", $("#files")[0].files[0]);
        /* Otra forma de hacerlo.
        $.each($('input[type=file]')[0].files, (i, file) => {
            data.append("AvatarImage", file);
        });
        */

        var url_base = window.location.protocol + "//" + window.location.host;
        var url_request = url_base + "/Cursos/GetCursos";
        $.ajax({
            url: url_request,
            data: data,
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                try {
                    var item = JSON.parse(result);

                    if (item.Code == "Done") {
                        window.location.href = url_base + "/Cursos/Index"
                    } else {
                        document.getElementById("mensaje").innerHTML = item.Description;
                    }
                } catch (e) {
                    document.getElementById("mensaje").innerHTML = result;
                }
            }
        });
    }

    EditarCurso(curso, cat) {
        $("#curNombre").val(curso.Nombre);
        $("#curDescripcion").val(curso.Descripcion);
        $("#curCosto").val(curso.Costo);
        $("#curHoras").val(curso.Horas);
        $("#curEstado").prop("checked", curso.Estado);
        $("#curCursoID").val(curso.CursoID);

        this.Image = curso.Image;

        document.getElementById("cursoImage").innerHTML = "<img class='cursoImage' src='data:image/jpg;base64," + curso.Image + "'/>";

        $("#curCategoria").val(curso.CategoriaID).change();

        /* Esta es otra forma más compleja de elegir el value selecionado del selector.
        let j = 1;
        let x = document.getElementById("curCategoria");
        x.options.length = 0;
        for (var i = 0; i < cat.length; i++) {
            if (cat[i].Value == curso.CategoriaID) {

                x.options[0] = new Option(cat[i].Text, cat[i].Value);
                x.selectedIndex = 0;
                j = i;
            } else {

                x.options[i] = new Option(cat[i].Text, cat[i].Value);
            }

        }
        x.options[j] = new Option(cat[0].Text, cat[0].Value);
        */
        console.log(curso);
    }

    ObtenerCurso(data) {
        console.log(data);
        document.getElementById("titleCursoEliminar").innerHTML = data.Nombre;
        this.CursoID = data.CursoID;
    }

    EliminarCurso() {
        var url_base = window.location.protocol + "//" + window.location.host;
        var url_request = url_base + "/Cursos/EliminarCurso";
        $.post({
            url: url_request,
            data: { CursoID: this.CursoID },
            success: function (response) {
                var item = JSON.parse(response);

                if (item.Code == "Done") {
                    window.location.href = url_base + "/Cursos/Index?area=Cursos";
                } else {
                    document.getElementById("mensaje-eliminar").innerHTML = item.Description;
                }
            }
        });
    }

    LimpiarCamposModal() {
        $("#curNombre").val("");
        $("#curDescripcion").val("");
        $("#curCosto").val("");
        $("#curHoras").val("");
        $("#curEstado").prop("checked", false);
        $("#curCursoID").val("");

        $("#curCategoria").val("");

        document.getElementById("cursoImage").innerHTML = '<img src="/Images/logo-google.png" class="cursoImage" />';
    }
}