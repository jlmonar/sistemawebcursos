﻿class Categorias {
    constructor() {
        this.CategoriaID = 0;
    }

    RegistrarCategoria() {
        var url_base = window.location.protocol + "//" + window.location.host;
        var url_request = url_base + "/Categorias/GetCategorias";
        $.post({
            url: url_request,
            data: $(".form-categoria").serialize(),
            success: function (response) {
                try {
                    var item = JSON.parse(response);
                    if (item.Code == "Done") {
                        window.location.href = url_base + "/categorias?area=Categorias"
                    } else {
                        document.getElementById("mensaje").innerHTML = item.Description;
                    }
                } catch (e) {
                    document.getElementById("mensaje").innerHTML = response;
                }
                console.log(response);LimpiarCamposModal()
            }
        });
    }

    EditarCategoria(data) {
        console.log(data);
        document.getElementById("catNombre").value = data.Nombre;
        document.getElementById("catDescripcion").value = data.Descripcion;
        document.getElementById("catEstado").checked = data.Estado;
        document.getElementById("catCategoriaID").value = data.CategoriaID;
    }

    //Método usado para limpiar los campos de texto del modal "Crear categoria" para que limpie los datos en los inputs
    //en caso de que antes tratar de insertar un registro, se lleno los campos del modal con los datos de alguna 
    //categoria que se quería editar.
    LimpiarCamposModal() {
        document.getElementById("catNombre").value = "";
        document.getElementById("catDescripcion").value = "";
        document.getElementById("catEstado").checked = true;
        document.getElementById("catCategoriaID").value = 0;
    }

    ObtenerCategoria(data) {
        this.CategoriaID = data.CategoriaID;
        document.getElementById("titleCategoriaEliminar").innerHTML = data.Nombre;
    }

    EliminarCategoria() {
        var url_base = window.location.protocol + "//" + window.location.host;
        var url_request = url_base + "/Categorias/EliminarCategoria";
        $.post({
            url: url_request,
            data: { CategoriaID: this.CategoriaID },
            success: function (response) {
                var item = JSON.parse(response);

                if (item.Code == "Done") {
                    window.location.href = url_base + "/categorias?area=categorias"
                } else {
                    document.getElementById("mensaje-eliminar").innerHTML = item.Description;
                }
            }
        });
    }
}