﻿using SistemaWebCursos.Areas.Categorias.Models;
using SistemaWebCursos.Areas.Inscripciones.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SistemaWebCursos.Areas.Cursos.Models
{
    public class TCursos
    {
        [Key]
        public int CursoID { get; set; }
        [Required(ErrorMessage = "El campo nombre es requerido.")]
        public String Nombre { get; set; }
        [Required(ErrorMessage = "El campo descripción es requerido.")]
        public String Descripcion {get;set;}
        [Required(ErrorMessage = "El campo horas es requerido.")]
        public byte Horas { get; set; }
        [Required(ErrorMessage = "El campo costo es requerido.")]
        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "El precio no es correcto.")]
        public decimal Costo { get; set; }
        public Boolean Estado { get; set; }
        public byte[] Image { get; set; }
        [Required(ErrorMessage = "Seleccione una categoria.")]
        public int CategoriaID { get; set; }
        /* Con estas etiquetas hago que esta propiedad sea ignorada durante srialización, para asi evitar posibles
         errores de referencias circulares, ya que categoria también tiene referencia a muchos cursos.*/
        [JsonIgnore]
        [IgnoreDataMember]
        public TCategoria Categoria { get; set; }
        public ICollection<Inscripcion> Inscripciones { get; set; }
    }
}
