﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaWebCursos.Areas.Cursos.Models;
using SistemaWebCursos.Data;
using SistemaWebCursos.Library;
using SistemaWebCursos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Areas.Cursos.Controllers
{
    [Area("Cursos")]
    [Authorize(Roles = "Admin")]
    public class CursosController : Controller
    {
        private LCursos _lcurso;
        private LCategorias _lcategoria;
        private SignInManager<IdentityUser> _signInManager;
        private static DataPaginador<TCursos> models;
        private static IdentityError identityError = null;
        private ApplicationDbContext context;

        public CursosController(ApplicationDbContext context, SignInManager<IdentityUser> signInManager, IWebHostEnvironment environment)
        {
            _signInManager = signInManager;
            _lcategoria = new LCategorias(context);
            _lcurso = new LCursos(context, environment);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index(int Id, String Search, int Registros)
        {
            if (_signInManager.IsSignedIn(User))
            {
                Object[] objects = new Object[3];
                var data = _lcurso.getTCursos(Search);

                if (data.Count > 0)
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    objects = new LPaginador<TCursos>().paginador(data, Id, Registros,
                        "Cursos", "Cursos", "Index", url);
                } else
                {
                    objects[0] = "No hay datos que mostrar.";
                    objects[1] = "No hay datos que mostrar.";
                    objects[2] = new List<TCursos>();
                }

                models = new DataPaginador<TCursos>
                {
                    Pagi_info = (String) objects[0],
                    Pagi_navegacion = (String) objects[1],
                    List = (List<TCursos>) objects[2],
                    Categorias = _lcategoria.getTCategorias(),
                    Input = new TCursos()
                };

                if (identityError != null)
                {
                    models.Pagi_info = identityError.Description;
                    identityError = null;
                }
            }
            return View(models);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public String GetCursos(DataPaginador<TCursos> model)
        {
            if (model.Input.Nombre != null && model.Input.Descripcion != null && model.Input.CategoriaID > 0)
            {
                if (model.Input.Horas.Equals(0))
                {
                    return "Ingrese las horas.";
                }

                if (model.Input.Costo.Equals(0.00M))
                {
                    return "Ingrese el costo.";
                }

                var data = _lcurso.RegistrarCursoAsync(model);
                identityError = data.Result;

                //Uso data.Result porque debo obtener el resultado de una tarea (Task)
                return JsonConvert.SerializeObject(data.Result);
            }
            return "Hola";
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateEstado(int id)
        {
            identityError =  _lcurso.UpdateEstado(id);
            return Redirect("/Cursos/Index?area=Cursos");
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public String EliminarCurso(int CursoID)
        {
            identityError = _lcurso.DeleteCurso(CursoID);
            return JsonConvert.SerializeObject(identityError);
        }
    }
}
