﻿using SistemaWebCursos.Areas.Cursos.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SistemaWebCursos.Areas.Categorias.Models
{
    public class TCategoria
    {
        [Key]
        public int CategoriaID { get; set; }
        [Required(ErrorMessage = "El nombre es requerido.")]
        public String Nombre { get; set; }
        [Required(ErrorMessage ="La descripción es requerida.")]
        public String Descripcion { get; set; }
        public Boolean Estado { get; set; } = true;
        public ICollection<TCursos> Cursos { get; set; }
    }
}
