﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaWebCursos.Areas.Categorias.Models;
using SistemaWebCursos.Controllers;
using SistemaWebCursos.Data;
using SistemaWebCursos.Library;
using SistemaWebCursos.Models;
using System;
using System.Collections.Generic;

namespace SistemaWebCursos.Areas.Categorias.Controllers
{
    [Area("Categorias")]
    [Authorize(Roles = "Admin")]
    public class CategoriasController : Controller
    {
        private TCategoria _categoria;
        private LCategorias _lcategoria;
        private SignInManager<IdentityUser> _signInManager;
        private static DataPaginador<TCategoria> models;
        private static IdentityError identityError = null;

        public CategoriasController(ApplicationDbContext context, SignInManager<IdentityUser> signInManager)
        {
            _signInManager = signInManager;
            _lcategoria = new LCategorias(context);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Número de la página de registros que queremos visualizar</param>
        /// <param name="Search">Texto de búsqueda que el usuario proporciona para filtrar la lista de registros</param>
        /// <param name="Registros">Número de registros (del modelo) que queremos mostrar en la vista</param>
        /// <returns>Se muestra la vista Index</returns>
        public IActionResult Index(int id, String Search, int Registros)
        {
            // Solo si el usuario está logueado podremos ingresar a la pantalla Index de categorias.
            if (_signInManager.IsSignedIn(User))
            {
                Object[] objects = new Object[3];
                var data = _lcategoria.getTCategorias(Search);

                //Si es mayor a 0,quiere decir que la consulta si obtuvo resultados.
                if (data.Count > 0)
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    objects = new LPaginador<TCategoria>().paginador(data, id, Registros,
                        "Categorias", "Categorias", "Index", url);
                }
                else
                {
                    objects[0] = "No hay datos que mostrar.";
                    objects[1] = "No hay datos que mostrar.";
                    objects[2] = new List<TCategoria>();
                }

                models = new DataPaginador<TCategoria>
                {
                    Pagi_info = (String) objects[0],
                    Pagi_navegacion = (String) objects[1],
                    List = (List<TCategoria>) objects[2],
                    Input = new TCategoria()
                };

                //La variable identityError es estática, por lo tanto,la información contenida en esta variable se
                //va a mantener en todo momento. En este caso, si es diferente a null vamos a mostrar la información del
                //error en la vista mediante models.Pagi_info, luego de pasar el dato del error al model, tenemos que 
                //dejar en null otra vez la variable estatica identityError para que la próxima vez que accedamos a la vista,
                //no vuelva a mostrarse el mensaje de error.
                if (identityError != null)
                {
                    models.Pagi_info = identityError.Description;
                    identityError = null;
                }
               return View(models);
            }
            else
            {
                // Caso contrario, se lo redirecciona a la vista Home.
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }

        }

        [HttpPost]
        public String GetCategorias(DataPaginador<TCategoria> model)
        {
            if (model.Input.Nombre != null && model.Input.Descripcion != null)
            {
                var data = _lcategoria.RegistrarCategoria(model.Input);
                return JsonConvert.SerializeObject(data);
            } else
            {
                return "Ingrese los datos requeridos.";
            }
        }

        /// <summary>
        /// Actualiza el estado de una categoria y retorna a la vista porincipal de Categorias.
        /// </summary>
        /// <param name="id">Identificador de la categoria a la que se le quiere actualizar el estado (Ojo, 
        /// el nombre de este parametro  debe ser igual al 'name' de uno de los inputs del form desde donde
        /// se llame a este método.)</param>
        /// <returns>Redireccion a la vista de categorias</returns>
        [HttpPost]
        public IActionResult UpdateEstado(int id)
        {
            identityError = _lcategoria.UpdateEstado(id);
            return Redirect("/Categorias/Index?area=Categorias");
        }

        [HttpPost]
        public String EliminarCategoria(int CategoriaID)
        {
            var data = _lcategoria.DeleteCategoria(CategoriaID);

            return JsonConvert.SerializeObject(data);
        }
    }
}
