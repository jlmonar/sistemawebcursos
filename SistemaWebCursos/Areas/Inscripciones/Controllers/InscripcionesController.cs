﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaWebCursos.Data;
using SistemaWebCursos.Library;
using SistemaWebCursos.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SistemaWebCursos.Areas.Inscripciones.Controllers
{
    [Area("Inscripciones")]
    public class InscripcionesController : Controller
    {
        private LCursos _lcurso;
        private SignInManager<IdentityUser> _signInManager;
        private UserManager<IdentityUser> _userManager;
        private static DataPaginador<DataCurso> model;
        private static IdentityError identityError;
        private ApplicationDbContext _context;
        private IWebHostEnvironment _environment;

        public InscripcionesController(ApplicationDbContext context, SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager,
                            IWebHostEnvironment environment)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _environment = environment;

            _lcurso = new LCursos(context, null);
        }

        public async Task<IActionResult> Index(int id, int Registros, String search)
        {
            if (_signInManager.IsSignedIn(User))
            {
                Object[] objects = new Object[3];
                var user = await _userManager.GetUserAsync(User);
                var idUser = await _userManager.GetUserIdAsync(user);

                var data = _lcurso.InscripcionesEstudiante(idUser, search);

                if (data.Count > 0)
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    objects = new LPaginador<DataCurso>().paginador(data, id, Registros,
                                            "Inscripciones", "Inscripciones", "Index", url);
                }
                else
                {
                    objects[0] = "No hay información para mostrar";
                    objects[1] = "No hay información para mostrar";
                    objects[2] = new List<DataCurso>();
                }

                model = new DataPaginador<DataCurso>()
                {
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    List = (List<DataCurso>)objects[2],
                    Input = new DataCurso()
                };

            }
            return View(model);
        }

        public IActionResult Detalles(int id)
        {
            var curso = _lcurso.GetTCurso(id);
            return View(curso);
        }

        public async Task<IActionResult> Export()
        {
            var list = new List<String[]>();

            if (model.List.Count > 0)
            {
                foreach(var item in model.List)
                {
                    String[] data =
                    {
                        item.CursoID.ToString(),
                        item.Nombre,
                        item.Descripcion,
                        item.Horas.ToString(),
                        String.Format("${0:#,###,###,##0.00####}", item.Costo),
                        item.Estado.ToString(),
                        item.Categoria
                    };
                    list.Add(data);
                }
            }

            String[] titles = { "CursoID,", "Nombre", "Descripción", "Horas", "Costo", "Estado", "Categoria"};

            var exportData = new ExportData(list, "Inscripciones.xlsx", "Inscripciones", titles, _environment);

            return await exportData.ExportDataAsync();
        }
    }
}
