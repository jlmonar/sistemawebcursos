﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SistemaWebCursos.Areas.Cursos.Models;
using SistemaWebCursos.Data;
using SistemaWebCursos.Library;
using SistemaWebCursos.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IServiceProvider _serviceProvider;
        private LCursos _lcurso;
        private static DataPaginador<TCursos> model;
        private SignInManager<IdentityUser> _signInManager;
        private UserManager<IdentityUser> _userManager;
        private static DataCurso _dataCurso;
        private static IdentityError identityError;

        public HomeController(ILogger<HomeController> logger, IServiceProvider serviceProvider, ApplicationDbContext context,
            SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _lcurso = new LCursos(context, null);
            _logger = logger;
            //_serviceProvider = serviceProvider;
        }

        public IActionResult Index(int id, String Search)
        {
            Object[] objects = new Object[3];
            var data = _lcurso.getTCursos(Search);

            if (data.Count > 0)
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                objects = new LPaginador<TCursos>().paginador(data, id, 1, "", "Home", "Index", url);
            } else
            {
                objects[0] = "No hay datos para mostrar";
                objects[1] = "No hay datos para mostrar";
                objects[2] = new List<TCursos>();
            }

            model = new DataPaginador<TCursos>()
            {
                Pagi_info = (String)objects[0],
                Pagi_navegacion = (String)objects[1],
                List = (List<TCursos>)objects[2],
                Input = new TCursos()
            };

            if (identityError != null)
            {
                model.Pagi_info = identityError.Description;
                identityError = null;
            }

            //await CreateRolesAsync(_serviceProvider);
            return View(model);
        }

        public IActionResult Detalles(int id)
        {
            var model = _lcurso.GetTCurso(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Obtener(int cursoId, int vista)
        {
            if (_signInManager.IsSignedIn(User))
            {
                //Obtengo al usuario logueado en el sistema.
                var user = await _userManager.GetUserAsync(User);
                //Obtengo id del usuario logueado
                var idUser = await _userManager.GetUserIdAsync(user);

                //Inscribo al estudiante en el curso seleccionado.
                var data = _lcurso.InscripcionEstudianteCurso(cursoId, idUser);

                if (data.Code.Equals("Done"))
                {
                    //Si el usuario se inscribe con exito al curso, lo redirijo a la vista de inscripciones.
                    return Redirect("/Inscripciones/Index?area=Inscripciones");
                } else
                {
                    //Asigno la data a identityError para mostrar el mensaje de error en la vista correspondiente.
                    identityError = data;
                    if (vista == 1)
                    {
                        // Regreso a la vista /Home/Index que fue desde donde se hizo la petición para inscribirse al curso.
                        return Redirect("/Home/Index");
                    } else
                    {
                        // Obtengo la data del curso, le incluyo el mensaje de error, y lo muestro en la vista Detalles. 
                        _dataCurso = _lcurso.GetTCurso(cursoId);
                        _dataCurso.ErrorMessage = data.Description;

                        return View("Detalles", _dataCurso);
                    }
                }
            } else
            {
                return Redirect("/Identity/Account/Login");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task CreateRolesAsync(IServiceProvider serviceProvider)
        {
            // IServiceProvider es una interfaz que trabaja  con servicios registrados, por tal motivo usamos el método
            //GetRequiredService para poder obtener servicios.
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            
            
            String[] roles = { "Admin", "Student" };

            foreach(var rol in roles)
            {
                //Verifico si el rol ya existe, el método es asincrono, por lo tanto CreateRolesAsync debe ser asincrono.
                var roleExist = await roleManager.RoleExistsAsync(rol);

                if (!roleExist)
                {
                    //Creamos los roles en la base de datos usando el método asincrono CreateAsync, se almacena en la tabla AspNetRoles.
                    await roleManager.CreateAsync(new IdentityRole(rol));
                }
            }

            //var usuario = await userManager.FindByIdAsync("1e8cee34-3aa6-46f7-8ccd-8eab10bf0994");
            var usuario = await userManager.FindByEmailAsync("user@mail.com");
            await userManager.AddToRoleAsync(usuario, "Admin");
        }
    }
}
