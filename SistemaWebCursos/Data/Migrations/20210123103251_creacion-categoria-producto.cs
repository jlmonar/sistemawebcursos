﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaWebCursos.Data.Migrations
{
    public partial class creacioncategoriaproducto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "_TCategoria",
                columns: table => new
                {
                    CategoriaID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: false),
                    Descripcion = table.Column<string>(nullable: false),
                    Estado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TCategoria", x => x.CategoriaID);
                });

            migrationBuilder.CreateTable(
                name: "_TCurso",
                columns: table => new
                {
                    CursoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Horas = table.Column<byte>(nullable: false),
                    Costo = table.Column<decimal>(nullable: false),
                    Estado = table.Column<bool>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true),
                    CategoriaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__TCurso", x => x.CursoID);
                    table.ForeignKey(
                        name: "FK__TCurso__TCategoria_CategoriaID",
                        column: x => x.CategoriaID,
                        principalTable: "_TCategoria",
                        principalColumn: "CategoriaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX__TCurso_CategoriaID",
                table: "_TCurso",
                column: "CategoriaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "_TCurso");

            migrationBuilder.DropTable(
                name: "_TCategoria");
        }
    }
}
