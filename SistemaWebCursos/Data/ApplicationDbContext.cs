﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SistemaWebCursos.Areas.Categorias.Models;
using SistemaWebCursos.Areas.Cursos.Models;
using SistemaWebCursos.Areas.Inscripciones.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaWebCursos.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<TCategoria> _TCategoria { get; set; }
        public DbSet<TCursos> _TCurso { get; set; }
        public DbSet<Inscripcion> _TInscripcion { get; set; }
    }
}
