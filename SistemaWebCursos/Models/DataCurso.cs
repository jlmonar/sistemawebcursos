﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebCursos.Models
{
    public class DataCurso
    {
        public int CursoID { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public byte Horas { get; set; }
        public decimal Costo { get; set; }
        public Boolean Estado { get; set; }
        public byte[] Image { get; set; }
        public String Categoria { get; set; }
        public string ErrorMessage { get; set; }
    }
}
